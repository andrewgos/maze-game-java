import java.util.*;

public class MazeGenerator {
	/**
	 * A class that implements a randomized maze generation function.
	 * @param mazeSize - size of the maze as int
	 */
	public MazeGenerator(int mazeSize) {
		this.mazeSize = mazeSize;
		this.mazeXSize = mazeSize;
		this.mazeYSize = mazeSize;
		this.bitMatrixXSize = (mazeSize/2) - 1;
		this.bitMatrixYSize = (mazeSize/2) - 1;
		
		this.listOfObstacles = new ArrayList<Block>();
		this.emptyBlock = new Block(EMPTY);
		this.logBlock = new Block(LOG);
		this.rockBlock = new Block(ROCK);
		this.endBlock = new Block(END_BLOCK);
		this.victoryBannerBlock = new Block(VICTORY_BANNER);
		
		listOfObstacles.add(logBlock);
		listOfObstacles.add(rockBlock);
		
		this.maze = new Block[mazeYSize][mazeXSize];
		this.bitMatrix = new int[bitMatrixYSize][bitMatrixXSize];
		
		/**
		 * Initialize maze of empty blocks.
		 */
		for (int currentY = 0; currentY < maze[0].length; currentY++) {
			for (int currentX = 0; currentX < maze[0].length; currentX++) {
				maze[currentY][currentX] = emptyBlock;
			}
		}
		
		/**
		 * Initialize matrix of 0s.
		 */
		for (int currentY = 0; currentY < bitMatrixYSize; currentY++) {
			for (int currentX = 0; currentX < bitMatrixXSize; currentX++) {
				bitMatrix[currentY][currentX] = EMPTY;
			}
		}
	}
	
	/**
	 * Generate randomized maze from the randomized bit matrix.
	 * @param randomizedBitMatrix - randomized bit matrix as int[][]
	 * @return maze - randomized maze as Block[][]
	 */
	public Block[][] generateRandomizedMaze(int[][] randomizedBitMatrix) {
		int lastXTracker = 0;
		int lastYTracker = 0;
		
		Block obstacleBlock = listOfObstacles.get(0);
		
		for (int currentY = 0; currentY < randomizedBitMatrix[0].length; currentY++) {
			for (int currentX = 0; currentX < randomizedBitMatrix[0].length; currentX++) {
				Collections.shuffle(listOfObstacles);
				obstacleBlock = listOfObstacles.get(0);
				
				if ((randomizedBitMatrix[currentY][currentX] & 1) == 0) {
					maze[currentY*2][currentX*2] = obstacleBlock; 
					maze[currentY*2][(currentX*2) + 1] = obstacleBlock;
				} else {
					maze[currentY*2][currentX*2] = obstacleBlock; 
					maze[currentY*2][(currentX*2) + 1] = emptyBlock;
				}
				
				lastXTracker = (currentX*2) + 2;
			}
			maze[currentY*2][lastXTracker] = obstacleBlock;
			
			for (int currentX = 0; currentX < randomizedBitMatrix[0].length; currentX++) {
				Collections.shuffle(listOfObstacles);
				obstacleBlock = listOfObstacles.get(0);
				
				if ((randomizedBitMatrix[currentY][currentX] & 8) == 0) {
					maze[(currentY*2) + 1][currentX*2] = obstacleBlock; 
					maze[(currentY*2) + 1][(currentX*2) + 1] = emptyBlock;
				} else {
					maze[(currentY*2) + 1][currentX*2] = emptyBlock; 
					maze[(currentY*2) + 1][(currentX*2) + 1] = emptyBlock;
				}
			}
			maze[(currentY*2) + 1][lastXTracker] = obstacleBlock;
			
			lastYTracker = (currentY*2) + 2;
		}
		
		for (int x = 0; x <= lastXTracker; x++) {
			Collections.shuffle(listOfObstacles);
			obstacleBlock = listOfObstacles.get(0);
			
			maze[lastYTracker][x] = obstacleBlock;
		}
		
		if (mazeSize == EASY_DIFFICULTY_SIZE || mazeSize == NORMAL_DIFFICULTY_SIZE
				|| mazeSize == INSANE_DIFFICULTY_SIZE) {
			maze[maze.length - 3][maze.length - 2] = emptyBlock;
			maze[maze.length - 3][maze.length - 1] = endBlock;
		} else if (mazeSize == HARD_DIFFICULTY_SIZE) {
			maze[maze.length - 4][maze.length - 3] = emptyBlock;
			maze[maze.length - 4][maze.length - 2] = endBlock;
		}
		
		if (mazeSize == EASY_DIFFICULTY_SIZE || mazeSize == NORMAL_DIFFICULTY_SIZE
				|| mazeSize == INSANE_DIFFICULTY_SIZE) {
			maze[maze.length - 4][maze.length - 1] = victoryBannerBlock;
			maze[maze.length - 2][maze.length - 1] = victoryBannerBlock;
		} else if (mazeSize == HARD_DIFFICULTY_SIZE) {
			maze[maze.length - 5][maze.length - 2] = obstacleBlock;
			maze[maze.length - 3][maze.length - 2] = obstacleBlock;
			maze[maze.length - 5][maze.length - 1] = victoryBannerBlock;
			maze[maze.length - 3][maze.length - 1] = victoryBannerBlock;
		}
		
		return this.maze;
	}
	
	/**
	 * Generate randomized maze in bit matrix representation by using recursive backtrack algorithm.
	 * @param currentX - current x position as int
	 * @param currentY - current y position as int
	 */
	public void generateRandomizedMazeInBit(int currentX, int currentY) {
		direction[] arrayOfDirections = direction.values();
		Collections.shuffle(Arrays.asList(arrayOfDirections));
		
		for (direction currentDirection : arrayOfDirections) {
			int newX = currentX + currentDirection.dx;
			int newY = currentY + currentDirection.dy;
			
			if (newX >= 0 && newX < bitMatrixXSize && newY >= 0 && newY < bitMatrixYSize
					&& bitMatrix[newY][newX] == EMPTY) {
				bitMatrix[currentY][currentX] |= currentDirection.bit;
				bitMatrix[newY][newX] |= currentDirection.opposite.bit;
				
				generateRandomizedMazeInBit(newX, newY);
			}
		}
	}
	
	/**
	 * Get bit matrix representation of a maze.
	 * @return bitMatrix - randomized bit matrix as int[][]
	 */
	public int[][] getBitMatrix() {
		return this.bitMatrix;
	}
	
	/**
	 * An enumerator that represents directions and neighboring bits.
	 */
	private enum direction {
		N(NORTH_BIT, 0, -1), S(SOUTH_BIT, 0, 1), E(EAST_BIT, 1, 0), W(WEST_BIT, -1, 0);
		private final int bit;
		private final int dx;
		private final int dy;
		private direction opposite;
 
		static {
			N.opposite = S;
			S.opposite = N;
			E.opposite = W;
			W.opposite = E;
		}
 
		private direction(int bit, int dx, int dy) {
			this.bit = bit;
			this.dx = dx;
			this.dy = dy;
		}
	};
	
	private int mazeSize;
	private int mazeXSize;
	private int mazeYSize;
	private int bitMatrixXSize;
	private int bitMatrixYSize;
	
	private ArrayList<Block> listOfObstacles;
	private Block emptyBlock;
	private Block logBlock;
	private Block rockBlock;
	private Block endBlock;
	private Block victoryBannerBlock;
	
	private int[][] bitMatrix;
	private Block[][] maze;
	
	private static final int EMPTY = 0;
	private static final int LOG = 1;
	private static final int ROCK = 2;
	private static final int END_BLOCK = 3;
	private static final int VICTORY_BANNER = 4;
	
	private static final int NORTH_BIT = 1;
	private static final int SOUTH_BIT = 2;
	private static final int EAST_BIT = 4;
	private static final int WEST_BIT = 8;
	
	private static final int EASY_DIFFICULTY_SIZE = 14;
	private static final int NORMAL_DIFFICULTY_SIZE = 28;
	private static final int HARD_DIFFICULTY_SIZE = 35;
	private static final int INSANE_DIFFICULTY_SIZE = 50;
}
