import javax.swing.*;

public class GUIHelp {
	/**
	 * A class that represents the GUI for help menu.
	 */
	public GUIHelp() {
		ImageIcon helpImage = new ImageIcon("src/img/Help.jpeg");
		
        JOptionPane.showMessageDialog(null, "", "How To Play", 
        		JOptionPane.INFORMATION_MESSAGE, helpImage);   
	}
}
