public class Block {
	/**
	 * A class that represents a block of a maze.
	 * @param blockType - type of block as int
	 */
	public Block(int blockType) {
		this.blockType = blockType;
	}
	
	/**
	 * Get the type of block.
	 * @return this.blockType as int
	 */
	public int getBlockType() {
		return this.blockType;
	}
	
	private int blockType;
}
