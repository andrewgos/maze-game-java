import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Board extends JPanel implements ActionListener, KeyListener {
	/**
	 * A class that is responsible for painting the individual components of the maze board.
	 * @param size - size of the maze as int
	 * @param boardFrame - game board as JFrame
	 */
	public Board(int size, JFrame boardFrame) {
		this.mazeSize = size;
		this.boardFrame = boardFrame;
		
		this.graph = new AdjacencyMatrixMaze(mazeSize);
		this.timer.start();
		
		this.pathImage = new PathImage();
		this.obstacleImage = new ObstacleImage();
		this.playerImage = new PlayerImage();
		this.cowImage = new CowImage();
		this.dogImage = new DogImage();
		this.chickenImage = new ChickenImage();
		this.victoryBannerImage = new VictoryBannerImage();
		
		this.player = new PlayerControl(mazeSize, this);
		this.cow = new CowMovement(mazeSize, graph, this);
		this.dog = new DogMovement(mazeSize, graph, this);
		this.chicken = new ChickenMovement(mazeSize, graph, this);
		
		this.blockSize = WINDOW_SIZE/mazeSize;
		
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		this.hasShownDialog = false;
	}
	
	/**
	 * Paint components of the maze such as path, obstacles, player, animals, etc.
	 * @param g as Graphics
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Image soil = pathImage.getSoil(blockSize);
		Image log = obstacleImage.getLog(blockSize);
		Image rock = obstacleImage.getRock(blockSize);
		Image victoryBanner = victoryBannerImage.getVictoryBanner(blockSize); 
		
		Image playerUp = playerImage.getPlayerUp(blockSize);
		Image playerDown = playerImage.getPlayerDown(blockSize);
		Image playerRight = playerImage.getPlayerRight(blockSize);
		Image playerLeft = playerImage.getPlayerLeft(blockSize);
		
		Image cowUp = cowImage.getCowUp(blockSize);
		Image cowDown = cowImage.getCowDown(blockSize);
		Image cowRight = cowImage.getCowRight(blockSize);
		Image cowLeft = cowImage.getCowLeft(blockSize);
		
		Image dogUp = dogImage.getDogUp(blockSize);
		Image dogDown = dogImage.getDogDown(blockSize);
		Image dogRight = dogImage.getDogRight(blockSize);
		Image dogLeft = dogImage.getDogLeft(blockSize);
		
		Image chickenUp = chickenImage.getChickenUp(blockSize);
		Image chickenDown = chickenImage.getChickenDown(blockSize);
		Image chickenRight = chickenImage.getChickenRight(blockSize);
		Image chickenLeft = chickenImage.getChickenLeft(blockSize);
		
		int fogSize = NO_FOG;
		
		/**
		 * Fog would be activated when insane difficulty is chosen.
		 */
		if (mazeSize == INSANE_DIFFICULTY_SIZE) {
			fogSize = FOG_SIZE;
		}
		
		for (int currentY = 0; currentY < mazeSize; currentY++) {
			for (int currentX = 0; currentX < mazeSize; currentX++) {
				if (getRadius(player.getXPosition(), player.getYPosition(), currentX, currentY) < fogSize) {
					if ((graph.checkBlock(currentX, currentY).getBlockType() == EMPTY 
							|| graph.checkBlock(currentX, currentY).getBlockType() == END_BLOCK)
							&& player.getXPosition() == currentX && player.getYPosition() == currentY) {
						if (player.getPlayerOrientation().equals(UP)) {
							g.drawImage(playerUp, currentX*blockSize, currentY*blockSize, null);
						} else if (player.getPlayerOrientation().equals(DOWN)) {
							g.drawImage(playerDown, currentX*blockSize, currentY*blockSize, null);
						} else if (player.getPlayerOrientation().equals(RIGHT)) {
							g.drawImage(playerRight, currentX*blockSize, currentY*blockSize, null);
						} else if (player.getPlayerOrientation().equals(LEFT)) {
							g.drawImage(playerLeft, currentX*blockSize, currentY*blockSize, null);
						}
					} else if ((graph.checkBlock(currentX, currentY).getBlockType() == EMPTY 
							|| graph.checkBlock(currentX, currentY).getBlockType() == END_BLOCK)
							&& cow.getXPosition() == currentX && cow.getYPosition() == currentY) {
						if (cow.getCowOrientation().equals(UP)) {
							g.drawImage(cowUp, currentX*blockSize, currentY*blockSize, null);
						} else if (cow.getCowOrientation().equals(DOWN)) {
							g.drawImage(cowDown, currentX*blockSize, currentY*blockSize, null);
						} else if (cow.getCowOrientation().equals(RIGHT)) {
							g.drawImage(cowRight, currentX*blockSize, currentY*blockSize, null);
						} else if (cow.getCowOrientation().equals(LEFT)) {
							g.drawImage(cowLeft, currentX*blockSize, currentY*blockSize, null);
						}
					} else if ((graph.checkBlock(currentX, currentY).getBlockType() == EMPTY 
							|| graph.checkBlock(currentX, currentY).getBlockType() == END_BLOCK)
							&& dog.getXPosition() == currentX && dog.getYPosition() == currentY) {
						if (dog.getDogOrientation().equals(UP)) {
							g.drawImage(dogUp, currentX*blockSize, currentY*blockSize, null);
						} else if (dog.getDogOrientation().equals(DOWN)) {
							g.drawImage(dogDown, currentX*blockSize, currentY*blockSize, null);
						} else if (dog.getDogOrientation().equals(RIGHT)) {
							g.drawImage(dogRight, currentX*blockSize, currentY*blockSize, null);
						} else if (dog.getDogOrientation().equals(LEFT)) {
							g.drawImage(dogLeft, currentX*blockSize, currentY*blockSize, null);
						}
					} else if ((graph.checkBlock(currentX, currentY).getBlockType() == EMPTY 
							|| graph.checkBlock(currentX, currentY).getBlockType() == END_BLOCK)
							&& chicken.getXPosition() == currentX && chicken.getYPosition() == currentY) {
						if (chicken.getChickenOrientation().equals(UP)) {
							g.drawImage(chickenUp, currentX*blockSize, currentY*blockSize, null);
						} else if (chicken.getChickenOrientation().equals(DOWN)) {
							g.drawImage(chickenDown, currentX*blockSize, currentY*blockSize, null);
						} else if (chicken.getChickenOrientation().equals(RIGHT)) {
							g.drawImage(chickenRight, currentX*blockSize, currentY*blockSize, null);
						} else if (chicken.getChickenOrientation().equals(LEFT)) {
							g.drawImage(chickenLeft, currentX*blockSize, currentY*blockSize, null);
						}
					} else if (graph.checkBlock(currentX, currentY).getBlockType() == EMPTY
							|| graph.checkBlock(currentX, currentY).getBlockType() == END_BLOCK) {
						g.drawImage(soil, currentX*blockSize, currentY*blockSize, null);
					} else if (graph.checkBlock(currentX, currentY).getBlockType() == LOG) {
						g.drawImage(log, currentX*blockSize, currentY*blockSize, null);
					} else if (graph.checkBlock(currentX, currentY).getBlockType() == ROCK) {
						g.drawImage(rock, currentX*blockSize, currentY*blockSize, null);
					} else if (graph.checkBlock(currentX, currentY).getBlockType() == VICTORY_BLOCK) {
						g.drawImage(victoryBanner, currentX*blockSize, currentY*blockSize, null);
					}
				} else {
					g.setColor(Color.darkGray);
					g.fillRect(currentX*blockSize, currentY*blockSize, blockSize, blockSize);
				}
			}
		}
		
		if (graph.checkBlock(player.getXPosition(), player.getYPosition()).getBlockType() != END_BLOCK){
			hasShownDialog = false;
		}
		
		/**
		 * Goal state is when all animals are retrieved to their specific locations
		 * and the player has successfully reached the end block.
		 */
		if (graph.checkBlock(player.getXPosition(), player.getYPosition()).getBlockType() == END_BLOCK
				&& cow.getXPosition() == mazeSize - 1 && cow.getYPosition() == 1
				&& dog.getXPosition() == mazeSize - 1 && dog.getYPosition() == 2
				&& chicken.getXPosition() == mazeSize - 1 && chicken.getYPosition() == 3) {
			boardFrame.dispose();
			new GUIVictory();
		}
		else if(graph.checkBlock(player.getXPosition(), player.getYPosition()).getBlockType() == END_BLOCK
				&& !hasShownDialog ){
			hasShownDialog = true;
		    JOptionPane.showMessageDialog(null, "You have not collected all the animals!"
	                ,"OH NO!", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	/**
	 * Action performed by Board to repaint the board.
	 * @param event as ActionEvent
	 */
	public void actionPerformed (ActionEvent event) {
		repaint();
	}
	
	/**
	 * Detect the key pressed from keyboard.
	 * @param event as KeyEvent
	 */
	public void keyPressed(KeyEvent event) {
		int code = event.getKeyCode();
		
		if (code == KeyEvent.VK_UP) {
			player.up(graph);
		} else if (code == KeyEvent.VK_DOWN) {
			player.down(graph);
		} else if (code == KeyEvent.VK_RIGHT) {
			player.right(graph);
		} else if (code == KeyEvent.VK_LEFT) {
			player.left(graph);
		}
	}
	
	/**
	 * Get the player.
	 * @return this.player as PlayerControl
	 */
	public PlayerControl getPlayer() {
		return this.player;
	}
	
	/**
	 * Get the cow.
	 * @return this.cow as CowMovement
	 */
	public CowMovement getCow() {
		return this.cow;
	}
	
	/**
	 * Get the dog.
	 * @return this.dog as DogMovement
	 */
	public DogMovement getDog() {
		return this.dog;
	}
	
	/**
	 * Get the chicken.
	 * @return this.chicken as ChickenMovement
	 */
	public ChickenMovement getChicken() {
		return this.chicken;
	}
	
	/**
	 * Retrieve the cow.
	 */
	public void retrieveCow() {
		this.cow.returnCow();
	}
	
	/**
	 * Retrieve the dog.
	 */
	public void retrieveDog() {
		this.dog.returnDog();
	}
	
	/**
	 * Retrieve the chicken.
	 */
	public void retrieveChicken() {
		this.chicken.returnChicken();
	}
	
	public void keyReleased(KeyEvent event) {
		//NOT USED.
	}

	public void keyTyped(KeyEvent event) {
		//NOT USED.
	}
	
	/**
	 * Return the radius from a current x and y position.
	 * @param currentX as int
	 * @param currentY as int
	 * @param tempX as int
	 * @param tempY as int
	 * @return radius as double
	 */
	private double getRadius (int currentX, int currentY, int tempX, int tempY) {
		double radius = 0;
		int dx = 0;
		int dy = 0;
		
		if (tempX > currentX) {
			dx = tempX - currentX;
		} else {
			dx = currentX - tempX;
		}
		
		if (tempY > currentY) {
			dy = tempY - currentY;
		} else {
			dy = currentY - tempY;
		}
		
		radius = Math.sqrt(dx*dx + dy*dy);
				
		return radius;
	}
	
	private static final long serialVersionUID = 1L;
	
	private int mazeSize;
	private JFrame boardFrame;
	
	private AdjacencyMatrixMaze graph;
	private Timer timer = new Timer(5, this);
	
	private PathImage pathImage;
	private ObstacleImage obstacleImage;
	private PlayerImage playerImage;
	private CowImage cowImage;
	private DogImage dogImage;
	private ChickenImage chickenImage;
	private VictoryBannerImage victoryBannerImage;
	
	private PlayerControl player;
	private CowMovement cow;
	private DogMovement dog;
	private ChickenMovement chicken;
	
	private int blockSize;
	private boolean hasShownDialog;
	
	private static final int WINDOW_SIZE = 700;
	
	private static final int EMPTY = 0;
	private static final int LOG = 1;
	private static final int ROCK = 2;
	private static final int END_BLOCK = 3;
	private static final int VICTORY_BLOCK = 4;
	
	private static final int INSANE_DIFFICULTY_SIZE = 50;
	
	private static final String UP = "up";
	private static final String DOWN = "down";
	private static final String RIGHT = "right";
	private static final String LEFT = "left";
			
	private static final int FOG_SIZE = 5;
	private static final int NO_FOG = 99999;
}
