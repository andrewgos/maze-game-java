import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUIMenu extends JPanel implements ActionListener {
	/**
	 * A class that represents the GUI for menu.
	 */
	public GUIMenu() {
		this.action = "";
		frame = new JFrame();
		
		ImageIcon gameImage = new ImageIcon("src/img/Maze.jpeg");
		JLabel gameLabel = new JLabel(gameImage);
		
		frame.setLayout(new FlowLayout());
		frame.setTitle("The Farm Maze");
		frame.setSize(MENU_SIZE, MENU_SIZE);
		frame.setLocationRelativeTo(null);
		frame.setBackground(Color.white);
		
		panel = new JPanel();
		panel.setSize(MENU_SIZE, MENU_SIZE);
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		ButtonImage button = new ButtonImage();
		
		JButton easyButton = new JButton(button.getDifficultyButtonImage(EASY_DIFFICULTY));
		easyButton.setBackground(Color.white);
		easyButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		JButton normalButton = new JButton(button.getDifficultyButtonImage(NORMAL_DIFFICULTY));
		normalButton.setBackground(Color.white);
		normalButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		JButton hardButton = new JButton(button.getDifficultyButtonImage(HARD_DIFFICULTY));
		hardButton.setBackground(Color.white);
		hardButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		JButton insaneButton = new JButton(button.getDifficultyButtonImage(INSANE_DIFFICULTY));
		insaneButton.setBackground(Color.white);
		insaneButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		JButton helpButton = new JButton(button.getHelpButtonImage());
		helpButton.setBackground(Color.white);
		helpButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		JButton quitButton = new JButton(button.getQuitButtonImage());
		quitButton.setBackground(Color.white);
		quitButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		
		easyButton.setActionCommand(EASY_DIFFICULTY);
		normalButton.setActionCommand(NORMAL_DIFFICULTY);
		hardButton.setActionCommand(HARD_DIFFICULTY);
		insaneButton.setActionCommand(INSANE_DIFFICULTY);
		helpButton.setActionCommand(HELP);
		quitButton.setActionCommand(QUIT_GAME);
		
		easyButton.addActionListener(this);
		normalButton.addActionListener(this);
		hardButton.addActionListener(this);
		insaneButton.addActionListener(this);
		helpButton.addActionListener(this);
		quitButton.addActionListener(this);
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		panel.add(easyButton, gbc);
		panel.add(normalButton, gbc);
		panel.add(hardButton, gbc);
		panel.add(insaneButton, gbc);
		panel.add(helpButton, gbc);
		panel.add(quitButton, gbc);
		
		frame.add(panel);
		frame.add(gameLabel);
		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);

		frame.getContentPane().setBackground(Color.white);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Action performed by GUIMenu.
	 * @param action - action made by the buttons in the menu as ActionEvent
	 */
	public void actionPerformed(ActionEvent action) {
		if (action.getActionCommand().equals(EASY_DIFFICULTY)) {
			this.action = EASY_DIFFICULTY;
			GUIGame.run();
		} else if (action.getActionCommand().equals(NORMAL_DIFFICULTY)) {
			this.action = NORMAL_DIFFICULTY;
			GUIGame.run();
		} else if (action.getActionCommand().equals(HARD_DIFFICULTY)) {
			this.action = HARD_DIFFICULTY;
			GUIGame.run();
		} else if (action.getActionCommand().equals(INSANE_DIFFICULTY)) {
			this.action = INSANE_DIFFICULTY;
			GUIGame.run();
		} else if (action.getActionCommand().equals(HELP)) {
			new GUIHelp();
		} else if (action.getActionCommand().equals(QUIT_GAME)) {
			this.disposeGUIMenu();
		}
	}
	
	/**
	 * Dispose the menu window.
	 */
	public void disposeGUIMenu() {
		frame.dispose();
	}
	
	/**
	 * Get the action made by the buttons in the menu.
	 * @return action as String
	 */
	public String getAction() {
		return this.action;
	}
	
	private static final long serialVersionUID = 1L;
	
	private JFrame frame;
	private JPanel panel;
	private String action;
	
	private static final int MENU_SIZE = 1000;
	
	private static final String EASY_DIFFICULTY = "easy";
	private static final String NORMAL_DIFFICULTY = "normal";
	private static final String HARD_DIFFICULTY = "hard";
	private static final String INSANE_DIFFICULTY = "insane";
	private static final String HELP = "help";
	private static final String QUIT_GAME = "quit";
}
