public class AdjacencyMatrixMaze implements MazeGraph {
	/**
	 * A class that represents and stores a maze as a adjacency matrix graph representation.
	 * The maze would be represented and stored as a Block[][].
	 * @param size - size of the maze as int
	 */
	public AdjacencyMatrixMaze(int size) {
		this.xSize = size;
		this.ySize = size;
		
		this.maze = new Block[ySize][xSize];
		this.emptyBlock = new Block(EMPTY);
		
		MazeGenerator generatedMaze = new MazeGenerator(size);
		generatedMaze.generateRandomizedMazeInBit(0, 0);
		int[][] randomizedBitMatrix = generatedMaze.getBitMatrix();
		
		maze = generatedMaze.generateRandomizedMaze(randomizedBitMatrix);
	}
	
	/**
	 * Add an obstacle to the given position in the graph.
	 * @param obstacleType - type of obstacle as Block
	 * @param x - x position as int
	 * @param y - y position as int
	 * @return true if obstacle is added and false otherwise as boolean
	 */
	public boolean addObstacle(Block obstacleType, int x, int y) {
		boolean output = false;
		
		if (this.checkBlock(x, y).getBlockType() == EMPTY) {
			maze[y][x] = obstacleType;
			output = true;
		} else {
			output = false;
		}
		
		return output;
	}

	/**
	 * Remove any obstacle on the given position in the graph.
	 * @param x - x position as int
	 * @param y - y position as int
	 * @return true if obstacle is removed and false otherwise as boolean
	 */
	public boolean removeObstacle(int x, int y) {
		boolean output = false;
		
		if (this.checkBlock(x, y).getBlockType() == LOG
				|| this.checkBlock(x, y).getBlockType() == ROCK) {
			maze[y][x] = emptyBlock;
			output = true;
		} else {
			output = false;
		}
		
		return output;
	}
	
	/**
	 * Check the type of Block present on the given position in the graph.
	 * @param x - x position as int
	 * @param y - y position as int
	 * @return maze[y][x] as Block
	 */
	public Block checkBlock (int x, int y) {
		return maze[y][x];
	}
	
	private int xSize;
	private int ySize;
	
	private Block[][] maze;
	private Block emptyBlock;
	
	private static final int EMPTY = 0;
	private static final int LOG = 1;
	private static final int ROCK = 2;
}