import java.awt.*;
import javax.swing.*;

public class ObstacleImage {
	/**
	 * A class that is responsible for storing all images of obstacles.
	 * Obstacles can be represented as log or rock in this case.
	 */
	public ObstacleImage() {
		this.easyLogImage = new ImageIcon("src/img/easyLog.jpeg");
		this.normalLogImage = new ImageIcon("src/img/normalLog.jpeg");
		this.hardLogImage = new ImageIcon("src/img/hardLog.jpeg");
		this.insaneLogImage = new ImageIcon("src/img/insaneLog.jpeg");
		
		this.easyRockImage = new ImageIcon("src/img/easyRock.jpeg");
		this.normalRockImage = new ImageIcon("src/img/normalRock.jpeg");
		this.hardRockImage = new ImageIcon("src/img/hardRock.jpeg");
		this.insaneRockImage = new ImageIcon("src/img/insaneRock.jpeg");
	}

	/**
	 * Get the image of a log with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.log - image of log as Image
	 */
	public Image getLog(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			log = easyLogImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			log = normalLogImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			log = hardLogImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			log = insaneLogImage.getImage();
		}
		 
		return this.log;
	}
	
	/**
	 * Get the image of a rock with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.rock - image of rock as Image
	 */
	public Image getRock(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			rock = easyRockImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			rock = normalRockImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			rock = hardRockImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			rock = insaneRockImage.getImage();
		}
		 
		return this.rock;
	}

	private ImageIcon easyLogImage;
	private ImageIcon normalLogImage;
	private ImageIcon hardLogImage;
	private ImageIcon insaneLogImage;
	
	private ImageIcon easyRockImage;
	private ImageIcon normalRockImage;
	private ImageIcon hardRockImage;
	private ImageIcon insaneRockImage;
	
	private Image log;
	private Image rock;
	
	private static final int EASY_DIFFICULTY = 50;
	private static final int NORMAL_DIFFICULTY = 25;
	private static final int HARD_DIFFICULTY = 20;
	private static final int INSANE_DIFFICULTY = 14;
}
