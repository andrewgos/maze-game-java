/**
 * An interface for graph that represents the maze.
 */
public interface MazeGraph {
	/**
	 * Add an obstacle to the given position in the graph.
	 * @param obstacleType - type of obstacle as Block
	 * @param x - x position as int
	 * @param y - y position as int
	 * @return true if obstacle is added and false otherwise as boolean
	 */
	public boolean addObstacle(Block obstacleType, int x, int y);

	/**
	 * Remove any obstacle on the given position in the graph.
	 * @param x - x position as int
	 * @param y - y position as int
	 * @return true if obstacle is removed and false otherwise as boolean
	 */
	public boolean removeObstacle(int x, int y);
	
	/**
	 * Check the type of Block present on the given position in the graph.
	 * @param x - x position as int
	 * @param y - y position as int
	 * @return Block 
	 */
	public Block checkBlock(int x, int y);
}