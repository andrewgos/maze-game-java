import java.util.Random;
import java.awt.event.*;
import javax.swing.*;

public class CowMovement implements Animal, ActionListener {
	/**
	 * A class that is responsible for cow's randomized movements.
	 * @param mazeSize - size of the maze as int
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @param board - maze board as Board
	 */
	public CowMovement(int mazeSize, AdjacencyMatrixMaze graph, Board board) {
		this.mazeSize = mazeSize;
		this.graph = graph;
		this.board = board;

		this.timer.start();
		Random random = new Random();
		int startingXPosition = random.nextInt(mazeSize - 3);
		int startingYPosition = random.nextInt(mazeSize - 3);
		
		while (graph.checkBlock(startingXPosition, startingYPosition).getBlockType() != EMPTY
				|| (startingXPosition < mazeSize/2 && startingYPosition < mazeSize/2)){
			random = new Random();
			startingXPosition = random.nextInt(mazeSize - 3);
			startingYPosition = random.nextInt(mazeSize - 3);
		}
		
		this.xPosition = startingXPosition;
		this.yPosition = startingYPosition;
		this.cowOrientation = DOWN;
	}
	
	/**
	 * Return the cow to a specific position after being retrieved.
	 */
	public void returnCow() {
		timer.stop();
		this.xPosition = mazeSize - 1;
		this.yPosition = 1;
	}

	/**
	 * Move the cow up.
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @return output - true if the cow moves up and false otherwise as boolean
	 */
	public boolean up(AdjacencyMatrixMaze graph) {
		boolean output = false;
		
		if (yPosition > 0) {
			if (graph.checkBlock(xPosition, yPosition - 1).getBlockType() == EMPTY
					|| graph.checkBlock(xPosition, yPosition - 1).getBlockType() == END_BLOCK) {
				if (board.getPlayer().getXPosition() == xPosition 
						&& board.getPlayer().getYPosition() == yPosition - 1) {
					board.retrieveCow();
				} else if (board.getDog().getXPosition() == xPosition
						&& board.getDog().getYPosition() == yPosition - 1) {
					swap(board.getDog());
				} else if (board.getChicken().getXPosition() == xPosition
						&& board.getChicken().getYPosition() == yPosition - 1) {
					swap(board.getChicken());
				} else {
					cowOrientation = UP;
					yPosition--;
				}
				
				output = true;
			}
		}
		
		return output;
	}

	/**
	 * Move the cow down.
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @return output - true if the cow moves down and false otherwise as boolean
	 */
	public boolean down(AdjacencyMatrixMaze graph) {
		boolean output = false;
		
		if (yPosition < mazeSize-1) {
			if (graph.checkBlock(xPosition, yPosition + 1).getBlockType() == EMPTY
					|| graph.checkBlock(xPosition , yPosition + 1).getBlockType() == END_BLOCK) {
				if (board.getPlayer().getXPosition() == xPosition 
						&& board.getPlayer().getYPosition() == yPosition + 1) {
					board.retrieveCow();
				} else if (board.getDog().getXPosition() == xPosition
						&& board.getDog().getYPosition() == yPosition + 1) {
					swap(board.getDog());
				} else if (board.getChicken().getXPosition() == xPosition
						&& board.getChicken().getYPosition() == yPosition + 1) {
					swap(board.getChicken());
				} else {
					cowOrientation = DOWN;
					yPosition++;
				}
				
				output = true;
			}
		}
		
		return output;
	}
	
	/**
	 * Move the cow right.
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @return output - true if the cow moves right and false otherwise as boolean
	 */
	public boolean right(AdjacencyMatrixMaze graph) {
		boolean output = false;
		
		if (xPosition < mazeSize-1) {
			if (graph.checkBlock(xPosition + 1, yPosition).getBlockType() == EMPTY
					|| graph.checkBlock(xPosition + 1, yPosition).getBlockType() == END_BLOCK) {
				if (board.getPlayer().getXPosition() == xPosition + 1
						&& board.getPlayer().getYPosition() == yPosition) {
					board.retrieveCow();
				} else if (board.getDog().getXPosition() == xPosition + 1
						&& board.getDog().getYPosition() == yPosition) {
					swap(board.getDog());
				} else if (board.getChicken().getXPosition() == xPosition + 1
						&& board.getChicken().getYPosition() == yPosition) {
					swap(board.getChicken());
				} else {
					cowOrientation = RIGHT;
					xPosition++;
				}
				
				output = true;
			}
		}
		
		return output;
	}

	/**
	 * Move the cow left.
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @return output - true if the cow moves left and false otherwise as boolean
	 */
	public boolean left(AdjacencyMatrixMaze graph) {
		boolean output = false;
		
		if (xPosition > 0) {
			if (graph.checkBlock(xPosition - 1, yPosition).getBlockType() == EMPTY
					|| graph.checkBlock(xPosition - 1, yPosition).getBlockType() == END_BLOCK) {
				if (board.getPlayer().getXPosition() == xPosition - 1
						&& board.getPlayer().getYPosition() == yPosition) {
					board.retrieveCow();
				} else if (board.getDog().getXPosition() == xPosition - 1
						&& board.getDog().getYPosition() == yPosition) {
					swap(board.getDog());
				} else if (board.getChicken().getXPosition() == xPosition - 1
						&& board.getChicken().getYPosition() == yPosition) {
					swap(board.getChicken());
				} else {
					cowOrientation = LEFT;
					xPosition--;
				}
				
				output = true;
			}
		}
		
		return output;
	}
	
	/**
	 * Action performed by the cow.
	 * @param event as ActionEvent
	 */
	public void actionPerformed(ActionEvent event) {
		Random random = new Random();
		int randomisedNumber = random.nextInt(4);
		
		if (randomisedNumber == 0) {
			if (!left(graph)) {
				right(graph);
			}
		} else if (randomisedNumber == 1) {
			if (!up(graph)) {
				down(graph);
			}
		} else if (randomisedNumber == 2) {
			if (!right(graph)) {
				left(graph);
			}
		} else if (randomisedNumber == 3) {
			if (!down(graph)) {
				up(graph);
			}
		}
	}

	/**
	 * Get the current x position of the cow.
	 * @return this.xPosition as int
	 */
	public int getXPosition() {
		return this.xPosition;
	}

	/**
	 * Get the current y position of the cow.
	 * @return this.yPosition as int
	 */
	public int getYPosition() {
		return this.yPosition;
	}

	/**
	 * Get the directional orientation of the cow.
	 * @return this.cowOrientation as String
	 */
	public String getCowOrientation() {
		return this.cowOrientation;
	}
	
	/**
	 * Set the x position of the cow.
	 * @param xPosition - x position to set to as int
	 */
	public void setXPosition(int xPosition) {
		this.xPosition = xPosition;
	}
	
	/**
	 * Set the y position of the cow.
	 * @param yPosition - y position to set to as int
	 */
	public void setYPosition(int yPosition) {
		this.yPosition = yPosition;
	}
	
	/**
	 * Swap the cow's position with the other animal's position.
	 * @param animal - animal to swap to as Animal
	 */
	private void swap(Animal animal) {
		int tempX = getXPosition();
		int tempY = getYPosition();
		
		this.setXPosition(animal.getXPosition());
		this.setYPosition(animal.getYPosition());
		
		animal.setXPosition(tempX);
		animal.setYPosition(tempY);
	}
	
	private int mazeSize;
	private AdjacencyMatrixMaze graph;
	private Board board;
	
	private Timer timer = new Timer(300, this);
	private int xPosition;
	private int yPosition;
	private String cowOrientation;
	
	private static final int EMPTY = 0;
	private static final int END_BLOCK = 3;
	
	private static final String UP = "up";
	private static final String DOWN = "down";
	private static final String RIGHT = "right";
	private static final String LEFT = "left";
}
