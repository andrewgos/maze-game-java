import java.awt.*;
import javax.swing.*;

public class CowImage {
	/**
	 * A class that is responsible for storing all images of a cow.
	 */
	public CowImage() {
		this.easyCowUpImage = new ImageIcon("src/img/easyCowUp.jpeg");
		this.normalCowUpImage = new ImageIcon("src/img/normalCowUp.jpeg");
		this.hardCowUpImage = new ImageIcon("src/img/hardCowUp.jpeg");
		this.insaneCowUpImage = new ImageIcon("src/img/insaneCowUp.jpeg");
		
		this.easyCowDownImage = new ImageIcon("src/img/easyCowDown.jpeg");
		this.normalCowDownImage = new ImageIcon("src/img/normalCowDown.jpeg");
		this.hardCowDownImage = new ImageIcon("src/img/hardCowDown.jpeg");
		this.insaneCowDownImage = new ImageIcon("src/img/insaneCowDown.jpeg");
		
		this.easyCowRightImage = new ImageIcon("src/img/easyCowRight.jpeg");
		this.normalCowRightImage = new ImageIcon("src/img/normalCowRight.jpeg");
		this.hardCowRightImage = new ImageIcon("src/img/hardCowRight.jpeg");
		this.insaneCowRightImage = new ImageIcon("src/img/insaneCowRight.jpeg");
		
		this.easyCowLeftImage = new ImageIcon("src/img/easyCowLeft.jpeg");
		this.normalCowLeftImage = new ImageIcon("src/img/normalCowLeft.jpeg");
		this.hardCowLeftImage = new ImageIcon("src/img/hardCowLeft.jpeg");
		this.insaneCowLeftImage = new ImageIcon("src/img/insaneCowLeft.jpeg");
	}
	
	/**
	 * Get the image of a cow facing up with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.cowUp - image of cow facing up as Image
	 */
	public Image getCowUp(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			cowUp = easyCowUpImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			cowUp = normalCowUpImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			cowUp = hardCowUpImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			cowUp = insaneCowUpImage.getImage();
		}
		 
		return this.cowUp;
	}
	
	/**
	 * Get the image of a cow facing down with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.cowDown - image of cow facing down as Image
	 */
	public Image getCowDown(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			cowDown = easyCowDownImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			cowDown = normalCowDownImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			cowDown = hardCowDownImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			cowDown = insaneCowDownImage.getImage();
		}
		 
		return this.cowDown;
	}
	
	/**
	 * Get the image of a cow facing right with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.cowRight - image of cow facing right as Image
	 */
	public Image getCowRight(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			cowRight = easyCowRightImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			cowRight = normalCowRightImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			cowRight = hardCowRightImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			cowRight = insaneCowRightImage.getImage();
		}
		 
		return this.cowRight;
	}
	
	/**
	 * Get the image of a cow facing left with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.cowLeft - image of cow facing left as Image
	 */
	public Image getCowLeft(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			cowLeft = easyCowLeftImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			cowLeft = normalCowLeftImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			cowLeft = hardCowLeftImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			cowLeft = insaneCowLeftImage.getImage();
		}
		 
		return this.cowLeft;
	}
	
	private ImageIcon easyCowUpImage;
	private ImageIcon normalCowUpImage;
	private ImageIcon hardCowUpImage;
	private ImageIcon insaneCowUpImage;
	
	private ImageIcon easyCowDownImage;
	private ImageIcon normalCowDownImage;
	private ImageIcon hardCowDownImage;
	private ImageIcon insaneCowDownImage;
	
	private ImageIcon easyCowRightImage;
	private ImageIcon normalCowRightImage;
	private ImageIcon hardCowRightImage;
	private ImageIcon insaneCowRightImage;
	
	private ImageIcon easyCowLeftImage;
	private ImageIcon normalCowLeftImage;
	private ImageIcon hardCowLeftImage;
	private ImageIcon insaneCowLeftImage;
	
	private Image cowUp;
	private Image cowDown;
	private Image cowRight;
	private Image cowLeft;
	
	private static final int EASY_DIFFICULTY = 50;
	private static final int NORMAL_DIFFICULTY = 25;
	private static final int HARD_DIFFICULTY = 20;
	private static final int INSANE_DIFFICULTY = 14;
}
