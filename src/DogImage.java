import java.awt.*;
import javax.swing.*;

public class DogImage {
	/**
	 * A class that is responsible for storing all images of a dog.
	 */
	public DogImage() {
		this.easyDogUpImage = new ImageIcon("src/img/easyDogUp.jpeg");
		this.normalDogUpImage = new ImageIcon("src/img/normalDogUp.jpeg");
		this.hardDogUpImage = new ImageIcon("src/img/hardDogUp.jpeg");
		this.insaneDogUpImage = new ImageIcon("src/img/insaneDogUp.jpeg");
		
		this.easyDogDownImage = new ImageIcon("src/img/easyDogDown.jpeg");
		this.normalDogDownImage = new ImageIcon("src/img/normalDogDown.jpeg");
		this.hardDogDownImage = new ImageIcon("src/img/hardDogDown.jpeg");
		this.insaneDogDownImage = new ImageIcon("src/img/insaneDogDown.jpeg");
		
		this.easyDogRightImage = new ImageIcon("src/img/easyDogRight.jpeg");
		this.normalDogRightImage = new ImageIcon("src/img/normalDogRight.jpeg");
		this.hardDogRightImage = new ImageIcon("src/img/hardDogRight.jpeg");
		this.insaneDogRightImage = new ImageIcon("src/img/insaneDogRight.jpeg");
		
		this.easyDogLeftImage = new ImageIcon("src/img/easyDogLeft.jpeg");
		this.normalDogLeftImage = new ImageIcon("src/img/normalDogLeft.jpeg");
		this.hardDogLeftImage = new ImageIcon("src/img/hardDogLeft.jpeg");
		this.insaneDogLeftImage = new ImageIcon("src/img/insaneDogLeft.jpeg");
	}
	
	/**
	 * Get the image of a dog facing up with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.dogUp - image of dog facing up as Image
	 */
	public Image getDogUp(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			dogUp = easyDogUpImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			dogUp = normalDogUpImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			dogUp = hardDogUpImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			dogUp = insaneDogUpImage.getImage();
		}
		 
		return this.dogUp;
	}
	
	/**
	 * Get the image of a dog facing down with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.dogDown - image of dog facing down as Image
	 */
	public Image getDogDown(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			dogDown = easyDogDownImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			dogDown = normalDogDownImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			dogDown = hardDogDownImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			dogDown = insaneDogDownImage.getImage();
		}
		 
		return this.dogDown;
	}
	
	/**
	 * Get the image of a dog facing right with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.dogRight - image of dog facing right as Image
	 */
	public Image getDogRight(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			dogRight = easyDogRightImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			dogRight = normalDogRightImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			dogRight = hardDogRightImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			dogRight = insaneDogRightImage.getImage();
		}
		 
		return this.dogRight;
	}
	
	/**
	 * Get the image of a dog facing left with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.dogLeft - image of dog facing left as Image
	 */
	public Image getDogLeft(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			dogLeft = easyDogLeftImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			dogLeft = normalDogLeftImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			dogLeft = hardDogLeftImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			dogLeft = insaneDogLeftImage.getImage();
		}
		 
		return this.dogLeft;
	}
	
	private ImageIcon easyDogUpImage;
	private ImageIcon normalDogUpImage;
	private ImageIcon hardDogUpImage;
	private ImageIcon insaneDogUpImage;
	
	private ImageIcon easyDogDownImage;
	private ImageIcon normalDogDownImage;
	private ImageIcon hardDogDownImage;
	private ImageIcon insaneDogDownImage;
	
	private ImageIcon easyDogRightImage;
	private ImageIcon normalDogRightImage;
	private ImageIcon hardDogRightImage;
	private ImageIcon insaneDogRightImage;
	
	private ImageIcon easyDogLeftImage;
	private ImageIcon normalDogLeftImage;
	private ImageIcon hardDogLeftImage;
	private ImageIcon insaneDogLeftImage;
	
	private Image dogUp;
	private Image dogDown;
	private Image dogRight;
	private Image dogLeft;
	
	private static final int EASY_DIFFICULTY = 50;
	private static final int NORMAL_DIFFICULTY = 25;
	private static final int HARD_DIFFICULTY = 20;
	private static final int INSANE_DIFFICULTY = 14;
}
