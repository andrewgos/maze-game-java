/**
 * An interface that represents animals' movements.
 */
public interface Animal {
	/**
	 * Move the animal up.
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @return true if the animal moves up and false otherwise
	 */
	public boolean up(AdjacencyMatrixMaze graph);
	
	/**
	 * Move the animal down.
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @return true if the animal moves down and false otherwise
	 */
	public boolean down(AdjacencyMatrixMaze graph);

	/**
	 * Move the animal right.
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @return true if the animal moves right and false otherwise
	 */
	public boolean right(AdjacencyMatrixMaze graph);
	
	/**
	 * Move the animal left.
	 * @param graph - maze as AdjacencyMatrixMaze
	 * @return true if the animal moves left and false otherwise
	 */
	public boolean left(AdjacencyMatrixMaze graph);
	
	/**
	 * Get the current x position of the animal.
	 * @return x position as int
	 */
	public int getXPosition();
	
	/**
	 * Get the current y position of the animal.
	 * @return y position as int
	 */
	public int getYPosition();
	
	/**
	 * Set the x position of the animal
	 * @param xPosition - x position to set to as int
	 */
	public void setXPosition(int xPosition);
	
	/**
	 * Set the y position of the animal
	 * @param yPosition - y position to set to as int
	 */
	public void setYPosition(int yPosition);
}
