import javax.swing.*;

public class ButtonImage {
	/**
	 * A class that is responsible for storing all images of buttons.
	 */
	public ButtonImage() {
		this.easyButtonImage = new ImageIcon("src/img/easyButton.jpeg");
		this.normalButtonImage = new ImageIcon("src/img/normalButton.jpeg");
		this.hardButtonImage = new ImageIcon("src/img/hardButton.jpeg");
		this.insaneButtonImage = new ImageIcon("src/img/insaneButton.jpeg");
		this.helpButtonImage = new ImageIcon("src/img/helpButton.jpeg");
		this.exitButtonImage = new ImageIcon("src/img/exitButton.jpeg");
		this.quitButtonImage = new ImageIcon("src/img/quitButton.jpeg");
		this.okButtonImage = new ImageIcon("src/img/okButton.jpeg");
	}
	
	public ImageIcon getDifficultyButtonImage(String difficulty) {
		if (difficulty.equals(EASY_DIFFICULTY)) {
			return this.easyButtonImage;
		} else if (difficulty.equals(NORMAL_DIFFICULTY)) {
			return this.normalButtonImage;
		} else if (difficulty.equals(HARD_DIFFICULTY)) {
			return this.hardButtonImage;
		} else if (difficulty.equals(INSANE_DIFFICULTY)) {
			return this.insaneButtonImage;
		} else {
			return null;
		}
	}
	
	public ImageIcon getHelpButtonImage() {
		return this.helpButtonImage;
	}
	
	public ImageIcon getExitButtonImage() {
		return this.exitButtonImage;
	}
	
	public ImageIcon getQuitButtonImage() {
		return this.quitButtonImage;
	}
	
	public ImageIcon getOkButtonImage() {
		return this.okButtonImage;
	}
	
	private ImageIcon easyButtonImage;
	private ImageIcon normalButtonImage;
	private ImageIcon hardButtonImage;
	private ImageIcon insaneButtonImage;
	private ImageIcon helpButtonImage;
	private ImageIcon exitButtonImage;
	private ImageIcon quitButtonImage;
	private ImageIcon okButtonImage;
	
	private static final String EASY_DIFFICULTY = "easy";
	private static final String NORMAL_DIFFICULTY = "normal";
	private static final String HARD_DIFFICULTY = "hard";
	private static final String INSANE_DIFFICULTY = "insane";
}
