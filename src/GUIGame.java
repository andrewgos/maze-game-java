import javax.swing.*;

public class GUIGame extends JPanel{
	/**
	 * A class that is responsible for running the game.
	 */
	public GUIGame(){
		menu = new GUIMenu();
	}
	
	/**
	 * Run the game depending on the difficulty chosen.
	 */
	public static void run() {
		if (menu.getAction().equals(EASY_DIFFICULTY)){
			menu.disposeGUIMenu();
			new GUIBoard(EASY_DIFFICULTY);
		} else if (menu.getAction().equals(NORMAL_DIFFICULTY)){
			menu.disposeGUIMenu();
			new GUIBoard(NORMAL_DIFFICULTY);
		} else if (menu.getAction().equals(HARD_DIFFICULTY)){
			menu.disposeGUIMenu();
			new GUIBoard(HARD_DIFFICULTY);
		} else if (menu.getAction().equals(INSANE_DIFFICULTY)){
			menu.disposeGUIMenu();
			new GUIBoard(INSANE_DIFFICULTY);
		}
	}
	
	private static final long serialVersionUID = 1L;
	
	private static GUIMenu menu;
	
	private static final String EASY_DIFFICULTY = "easy";
	private static final String NORMAL_DIFFICULTY = "normal";
	private static final String HARD_DIFFICULTY = "hard";
	private static final String INSANE_DIFFICULTY = "insane";
}
