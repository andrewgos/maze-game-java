import java.awt.*;
import javax.swing.*;

public class VictoryBannerImage {
	/**
	 * A class that is responsible for storing all images of a victory banner.
	 */
	public VictoryBannerImage() {
		this.easyVictoryBannerImage = new ImageIcon("src/img/easyVictoryBanner.jpeg");
		this.normalVictoryBannerImage = new ImageIcon("src/img/normalVictoryBanner.jpeg");
		this.hardVictoryBannerImage = new ImageIcon("src/img/hardVictoryBanner.jpeg");
		this.insaneVictoryBannerImage = new ImageIcon("src/img/insaneVictoryBanner.jpeg");
	}

	/**
	 * Get the image of a victory banner with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.victoryBanner - image of victory banner as Image
	 */
	public Image getVictoryBanner(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			victoryBanner = easyVictoryBannerImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			victoryBanner = normalVictoryBannerImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			victoryBanner = hardVictoryBannerImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			victoryBanner = insaneVictoryBannerImage.getImage();
		}
		 
		return this.victoryBanner;
	}

	private ImageIcon easyVictoryBannerImage;
	private ImageIcon normalVictoryBannerImage;
	private ImageIcon hardVictoryBannerImage;
	private ImageIcon insaneVictoryBannerImage;
	
	private Image victoryBanner;
	
	private static final int EASY_DIFFICULTY = 50;
	private static final int NORMAL_DIFFICULTY = 25;
	private static final int HARD_DIFFICULTY = 20;
	private static final int INSANE_DIFFICULTY = 14;
}
