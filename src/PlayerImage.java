import java.awt.*;
import javax.swing.*;

public class PlayerImage {
	/**
	 * A class that is responsible for storing all images of a player.
	 */
	public PlayerImage() {
		this.easyPlayerUpImage = new ImageIcon("src/img/easyPlayerUp.jpeg");
		this.normalPlayerUpImage = new ImageIcon("src/img/normalPlayerUp.jpeg");
		this.hardPlayerUpImage = new ImageIcon("src/img/hardPlayerUp.jpeg");
		this.insanePlayerUpImage = new ImageIcon("src/img/insanePlayerUp.jpeg");
		
		this.easyPlayerDownImage = new ImageIcon("src/img/easyPlayerDown.jpeg");
		this.normalPlayerDownImage = new ImageIcon("src/img/normalPlayerDown.jpeg");
		this.hardPlayerDownImage = new ImageIcon("src/img/hardPlayerDown.jpeg");
		this.insanePlayerDownImage = new ImageIcon("src/img/insanePlayerDown.jpeg");
		
		this.easyPlayerRightImage = new ImageIcon("src/img/easyPlayerRight.jpeg");
		this.normalPlayerRightImage = new ImageIcon("src/img/normalPlayerRight.jpeg");
		this.hardPlayerRightImage = new ImageIcon("src/img/hardPlayerRight.jpeg");
		this.insanePlayerRightImage = new ImageIcon("src/img/insanePlayerRight.jpeg");
		
		this.easyPlayerLeftImage = new ImageIcon("src/img/easyPlayerLeft.jpeg");
		this.normalPlayerLeftImage = new ImageIcon("src/img/normalPlayerLeft.jpeg");
		this.hardPlayerLeftImage = new ImageIcon("src/img/hardPlayerLeft.jpeg");
		this.insanePlayerLeftImage = new ImageIcon("src/img/insanePlayerLeft.jpeg");
	}
	
	/**
	 * Get the image of a player facing up with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.playerUp - image of player facing up as Image
	 */
	public Image getPlayerUp(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			playerUp = easyPlayerUpImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			playerUp = normalPlayerUpImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			playerUp = hardPlayerUpImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			playerUp = insanePlayerUpImage.getImage();
		}
		 
		return this.playerUp;
	}
	
	/**
	 * Get the image of a player facing down with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.playerDown - image of player facing down as Image
	 */
	public Image getPlayerDown(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			playerDown = easyPlayerDownImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			playerDown = normalPlayerDownImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			playerDown = hardPlayerDownImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			playerDown = insanePlayerDownImage.getImage();
		}
		 
		return this.playerDown;
	}
	
	/**
	 * Get the image of a player facing right with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.playerRight - image of player facing right as Image
	 */
	public Image getPlayerRight(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			playerRight = easyPlayerRightImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			playerRight = normalPlayerRightImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			playerRight = hardPlayerRightImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			playerRight = insanePlayerRightImage.getImage();
		}
		 
		return this.playerRight;
	}
	
	/**
	 * Get the image of a player facing left with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.playerLeft - image of player facing left as Image
	 */
	public Image getPlayerLeft(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			playerLeft = easyPlayerLeftImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			playerLeft = normalPlayerLeftImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			playerLeft = hardPlayerLeftImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			playerLeft = insanePlayerLeftImage.getImage();
		}
		 
		return this.playerLeft;
	}
	
	private ImageIcon easyPlayerUpImage;
	private ImageIcon normalPlayerUpImage;
	private ImageIcon hardPlayerUpImage;
	private ImageIcon insanePlayerUpImage;
	
	private ImageIcon easyPlayerDownImage;
	private ImageIcon normalPlayerDownImage;
	private ImageIcon hardPlayerDownImage;
	private ImageIcon insanePlayerDownImage;
	
	private ImageIcon easyPlayerRightImage;
	private ImageIcon normalPlayerRightImage;
	private ImageIcon hardPlayerRightImage;
	private ImageIcon insanePlayerRightImage;
	
	private ImageIcon easyPlayerLeftImage;
	private ImageIcon normalPlayerLeftImage;
	private ImageIcon hardPlayerLeftImage;
	private ImageIcon insanePlayerLeftImage;
	
	private Image playerUp;
	private Image playerDown;
	private Image playerRight;
	private Image playerLeft;
	
	private static final int EASY_DIFFICULTY = 50;
	private static final int NORMAL_DIFFICULTY = 25;
	private static final int HARD_DIFFICULTY = 20;
	private static final int INSANE_DIFFICULTY = 14;
}
