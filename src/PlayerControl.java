public class PlayerControl {
	/**
	 * A class that is responsible for the control of the player.
	 * @param mazeSize - size of the maze as int
	 * @param board - maze board as Board
	 */
	public PlayerControl(int mazeSize, Board board) {
		this.mazeSize = mazeSize;
		this.board = board;
		
		this.xPosition = STARTING_X_POSITION;
		this.yPosition = STARTING_Y_POSITION;
		this.playerOrientation = DOWN;
	}

	/**
	 * Move the player up.
	 * @param graph -  maze as AdjacencyMatrixMaze
	 */
	public void up(AdjacencyMatrixMaze graph) {
		if (yPosition > 0) {
			if (graph.checkBlock(xPosition, yPosition - 1).getBlockType() == EMPTY
					|| graph.checkBlock(xPosition, yPosition - 1).getBlockType() == END_BLOCK) {
				playerOrientation = UP;
				yPosition--;
				animalRetrieval();
			} 
		}
	}

	/**
	 * Move the player down.
	 * @param graph -  maze as AdjacencyMatrixMaze
	 */
	public void down(AdjacencyMatrixMaze graph) {
		if (yPosition < mazeSize - 1) {
			if (graph.checkBlock(xPosition, yPosition + 1).getBlockType() == EMPTY
					|| graph.checkBlock(xPosition , yPosition + 1).getBlockType() == END_BLOCK) {
				playerOrientation = DOWN;
				yPosition++;
				animalRetrieval();
			}
		}
	}

	/**
	 * Move the player right.
	 * @param graph -  maze as AdjacencyMatrixMaze
	 */
	public void right(AdjacencyMatrixMaze graph) {
		if (xPosition < mazeSize - 1) {
			if (graph.checkBlock(xPosition + 1, yPosition).getBlockType() == EMPTY
					|| graph.checkBlock(xPosition + 1, yPosition).getBlockType() == END_BLOCK) {
				playerOrientation = RIGHT;
				xPosition++;
				animalRetrieval();
			}
		}
	}

	/**
	 * Move the player left.
	 * @param graph -  maze as AdjacencyMatrixMaze
	 */
	public void left(AdjacencyMatrixMaze graph) {
		if (xPosition > 0) {
			if (graph.checkBlock(xPosition - 1, yPosition).getBlockType() == EMPTY
					|| graph.checkBlock(xPosition - 1, yPosition).getBlockType() == END_BLOCK) {
				playerOrientation = LEFT;
				xPosition--;
				animalRetrieval();
			} 
		}
	}

	/**
	 * Get the current x position of the player.
	 * @return this.xPosition as int
	 */
	public int getXPosition() {
		return this.xPosition;
	}

	/**
	 * Get the current y position of the player.
	 * @return this.yPosition as int
	 */
	public int getYPosition() {
		return this.yPosition;
	}

	/**
	 * Get the directional orientation of the player.
	 * @return this.playerOrientation as String
	 */
	public String getPlayerOrientation() {
		return this.playerOrientation;
	}
	
	/**
	 * Retrieve animal when the player and animal are on the same position.
	 */
	private void animalRetrieval() {
		if (board.getDog().getXPosition() == xPosition 
				&& board.getDog().getYPosition() == yPosition) {
			board.retrieveDog();
		} else if (board.getCow().getXPosition() == xPosition 
				&& board.getCow().getYPosition() == yPosition) {
			board.retrieveCow();
		} else if (board.getChicken().getXPosition() == xPosition 
				&& board.getChicken().getYPosition() == yPosition) {
			board.retrieveChicken();
		}
	}
	
	private int mazeSize;
	private Board board;
	
	private int xPosition;
	private int yPosition;
	private String playerOrientation;

	private static final int EMPTY = 0;
	private static final int END_BLOCK = 3;
	
	private static final String UP = "up";
	private static final String DOWN = "down";
	private static final String RIGHT = "right";
	private static final String LEFT = "left";
	
	private static final int STARTING_X_POSITION = 1;
	private static final int STARTING_Y_POSITION = 1;
}
