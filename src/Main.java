public class Main {
	/**
	 * A class that holds the main of the game.
	 * @param args as String[]
	 */
	public static void main(String[] args) {
		new GUIGame();
	}
}
