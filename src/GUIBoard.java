import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class GUIBoard implements ActionListener {
	/**
	 * A class that represents the GUI for board game.
	 * @param difficulty as String
	 */
	public GUIBoard(String difficulty) {
		boardFrame = new JFrame();
		int mazeSize = 0;
		
		if (difficulty.equals(EASY_DIFFICULTY)) {
			mazeSize = EASY_DIFFICULTY_SIZE;
		} else if (difficulty.equals(NORMAL_DIFFICULTY)) {
			mazeSize = NORMAL_DIFFICULTY_SIZE;
		} else if (difficulty.equals(HARD_DIFFICULTY)) {
			mazeSize = HARD_DIFFICULTY_SIZE;
		} else if (difficulty.equals(INSANE_DIFFICULTY)) {
			mazeSize = INSANE_DIFFICULTY_SIZE;
		}
		
		ImageIcon characterImage = new ImageIcon("src/img/Character.jpeg");
		JLabel label = new JLabel(characterImage);
		
		sidePanel = new JPanel();
		sidePanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		ButtonImage button = new ButtonImage();
		helpButton = new JButton(button.getHelpButtonImage());
		helpButton.setBackground(Color.white);
		helpButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		exitButton = new JButton(button.getExitButtonImage());
		exitButton.setBackground(Color.white);
		exitButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
		
		helpButton.addActionListener(this);
		exitButton.addActionListener(this);
		
		gbc.fill = GridBagConstraints.VERTICAL;
		gbc.gridy = 1;
		sidePanel.add(label, gbc);
		gbc.gridy = 2;
		sidePanel.add(helpButton, gbc);
		gbc.gridy = 3;
		sidePanel.add(exitButton, gbc);
		
		sidePanel.setBackground(Color.white);
		sidePanel.setBorder(BorderFactory.createLineBorder(Color.black, 10));
		
		gamePanel = new Board(mazeSize, boardFrame);
		
		gamePanel.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent event) {
				gamePanel.requestFocus();
			}
		});
		
		boardFrame.add(sidePanel, BorderLayout.EAST);
		boardFrame.add(gamePanel);
		boardFrame.pack();
		boardFrame.setTitle("The Farm Maze");
		boardFrame.setSize((WINDOW_SIZE*5)/4, WINDOW_SIZE);
		boardFrame.setLocationRelativeTo(null);
		boardFrame.setVisible(true);
		boardFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		gamePanel.requestFocus();
	}
	
	/**
	 * Action performed by GUIBoard.
	 * @param event as ActionEvent
	 */
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == helpButton) {
			new GUIHelp();
		} else if (event.getSource() == exitButton) {
			boardFrame.dispose();
			new GUIGame();
		}
		
	}
	
	private JFrame boardFrame;
	private JPanel gamePanel;
	private JPanel sidePanel;
	
	private JButton helpButton;
	private JButton exitButton;
	
	private static final int WINDOW_SIZE = 715;
	
	private static final String EASY_DIFFICULTY = "easy";
	private static final String NORMAL_DIFFICULTY = "normal";
	private static final String HARD_DIFFICULTY = "hard";
	private static final String INSANE_DIFFICULTY = "insane";
	
	private static final int EASY_DIFFICULTY_SIZE = 14;
	private static final int NORMAL_DIFFICULTY_SIZE = 28;
	private static final int HARD_DIFFICULTY_SIZE = 35;
	private static final int INSANE_DIFFICULTY_SIZE = 50;
}
