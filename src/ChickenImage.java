import java.awt.*;
import javax.swing.*;

public class ChickenImage {
	/**
	 * A class that is responsible for storing all images of a chicken.
	 */
	public ChickenImage() {
		this.easyChickenUpImage = new ImageIcon("src/img/easyChickenUp.jpeg");
		this.normalChickenUpImage = new ImageIcon("src/img/normalChickenUp.jpeg");
		this.hardChickenUpImage = new ImageIcon("src/img/hardChickenUp.jpeg");
		this.insaneChickenUpImage = new ImageIcon("src/img/insaneChickenUp.jpeg");
		
		this.easyChickenDownImage = new ImageIcon("src/img/easyChickenDown.jpeg");
		this.normalChickenDownImage = new ImageIcon("src/img/normalChickenDown.jpeg");
		this.hardChickenDownImage = new ImageIcon("src/img/hardChickenDown.jpeg");
		this.insaneChickenDownImage = new ImageIcon("src/img/insaneChickenDown.jpeg");
		
		this.easyChickenRightImage = new ImageIcon("src/img/easyChickenRight.jpeg");
		this.normalChickenRightImage = new ImageIcon("src/img/normalChickenRight.jpeg");
		this.hardChickenRightImage = new ImageIcon("src/img/hardChickenRight.jpeg");
		this.insaneChickenRightImage = new ImageIcon("src/img/insaneChickenRight.jpeg");
		
		this.easyChickenLeftImage = new ImageIcon("src/img/easyChickenLeft.jpeg");
		this.normalChickenLeftImage = new ImageIcon("src/img/normalChickenLeft.jpeg");
		this.hardChickenLeftImage = new ImageIcon("src/img/hardChickenLeft.jpeg");
		this.insaneChickenLeftImage = new ImageIcon("src/img/insaneChickenLeft.jpeg");
	}
	
	/**
	 * Get the image of a chicken facing up with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.chickenUp - image of chicken facing up as Image
	 */
	public Image getChickenUp(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			chickenUp = easyChickenUpImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			chickenUp = normalChickenUpImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			chickenUp = hardChickenUpImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			chickenUp = insaneChickenUpImage.getImage();
		}
		 
		return this.chickenUp;
	}
	
	/**
	 * Get the image of a chicken facing down with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.chickenDown - image of chicken facing down as Image
	 */
	public Image getChickenDown(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			chickenDown = easyChickenDownImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			chickenDown = normalChickenDownImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			chickenDown = hardChickenDownImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			chickenDown = insaneChickenDownImage.getImage();
		}
		 
		return this.chickenDown;
	}
	
	/**
	 * Get the image of a chicken facing right with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.chickenRight - image of chicken facing right as Image
	 */
	public Image getChickenRight(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			chickenRight = easyChickenRightImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			chickenRight = normalChickenRightImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			chickenRight = hardChickenRightImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			chickenRight = insaneChickenRightImage.getImage();
		}
		 
		return this.chickenRight;
	}
	
	/**
	 * Get the image of a chicken facing left with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.chickenLeft - image of chicken facing left as Image
	 */
	public Image getChickenLeft(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			chickenLeft = easyChickenLeftImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			chickenLeft = normalChickenLeftImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			chickenLeft = hardChickenLeftImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			chickenLeft = insaneChickenLeftImage.getImage();
		}
		 
		return this.chickenLeft;
	}
	
	private ImageIcon easyChickenUpImage;
	private ImageIcon normalChickenUpImage;
	private ImageIcon hardChickenUpImage;
	private ImageIcon insaneChickenUpImage;
	
	private ImageIcon easyChickenDownImage;
	private ImageIcon normalChickenDownImage;
	private ImageIcon hardChickenDownImage;
	private ImageIcon insaneChickenDownImage;
	
	private ImageIcon easyChickenRightImage;
	private ImageIcon normalChickenRightImage;
	private ImageIcon hardChickenRightImage;
	private ImageIcon insaneChickenRightImage;
	
	private ImageIcon easyChickenLeftImage;
	private ImageIcon normalChickenLeftImage;
	private ImageIcon hardChickenLeftImage;
	private ImageIcon insaneChickenLeftImage;
	
	private Image chickenUp;
	private Image chickenDown;
	private Image chickenRight;
	private Image chickenLeft;
	
	private static final int EASY_DIFFICULTY = 50;
	private static final int NORMAL_DIFFICULTY = 25;
	private static final int HARD_DIFFICULTY = 20;
	private static final int INSANE_DIFFICULTY = 14;
}
