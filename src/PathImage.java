import java.awt.*;
import javax.swing.*;

public class PathImage {
	/**
	 * A class that is responsible for storing all images of a path.
	 * Path can be represented as soil in this case.
	 */
	public PathImage() {
		this.easySoilImage = new ImageIcon("src/img/easySoil.jpeg");
		this.normalSoilImage = new ImageIcon("src/img/normalSoil.jpeg");
		this.hardSoilImage = new ImageIcon("src/img/hardSoil.jpeg");
		this.insaneSoilImage = new ImageIcon("src/img/insaneSoil.jpeg");
	}
	
	/**
	 * Get the image of a soil with size depending on the difficulty.
	 * @param difficulty as String
	 * @return this.soil - image of soil as Image
	 */
	public Image getSoil(int difficulty) {
		if (difficulty == EASY_DIFFICULTY) {
			soil = easySoilImage.getImage();
		} else if (difficulty == NORMAL_DIFFICULTY) {
			soil = normalSoilImage.getImage();
		} else if (difficulty == HARD_DIFFICULTY) {
			soil = hardSoilImage.getImage();
		} else if (difficulty == INSANE_DIFFICULTY) {
			soil = insaneSoilImage.getImage();
		}
		 
		return this.soil;
	}

	private ImageIcon easySoilImage;
	private ImageIcon normalSoilImage;
	private ImageIcon hardSoilImage;
	private ImageIcon insaneSoilImage;
	
	private Image soil;
	
	private static final int EASY_DIFFICULTY = 50;
	private static final int NORMAL_DIFFICULTY = 25;
	private static final int HARD_DIFFICULTY = 20;
	private static final int INSANE_DIFFICULTY = 14;
}
