import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class GUIVictory implements ActionListener {
	/**
	 * A class that represents the GUI for victory pop-up window.
	 */
	public GUIVictory() {
		boardFrame = new JFrame();
		boardFrame.setLayout(new FlowLayout());
		
		ImageIcon victoryImage = new ImageIcon("src/img/Victory.jpeg");
		JLabel victoryLabel = new JLabel(victoryImage);
	    
		ButtonImage button = new ButtonImage();
	    JButton okButton = new JButton(button.getOkButtonImage());
	    okButton.setBackground(Color.white);
	    okButton.setBorder(BorderFactory.createLineBorder(Color.white, 1));
	    
		okButton.addActionListener(this);
		
		boardFrame.add(victoryLabel);
		boardFrame.add(okButton);
		boardFrame.setTitle("Congratulations!");
	    boardFrame.setBackground(Color.white);
	    boardFrame.pack();
	    boardFrame.setVisible(true);
	    
	    victoryLabel.requestFocus();
	}

	/**
	 * Action performed by GUIVictory.
	 * @param event as ActionEvent
	 */
	public void actionPerformed(ActionEvent event) {
		boardFrame.dispose();
		new GUIGame();
	}
	
	private JFrame boardFrame;
}
